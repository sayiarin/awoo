variables:
  VERSION: dev
  REPO_NAME: registry.gitlab.com/sayiarin/awoo
  DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$VERSION-$CI_COMMIT_SHORT_SHA
  ARTIFACT_DIR: artifacts/awoo-server

stages:
  - compile
  - build-docker-image
  - deploy-to-ec2

compile:
  stage: compile
  image: golang:1.17-alpine3.14
  before_script:
    - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
    - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
    - cd $GOPATH/src/$REPO_NAME/server
  script:
    - CGO_ENABLED=0 go build -o ../$ARTIFACT_DIR
  artifacts:
    paths:
     - $ARTIFACT_DIR
    expire_in: 1h

build:
  stage: build-docker-image
  dependencies:
   - compile
  image: docker:latest
  tags:
   - gitlab-org-docker
  services:
   - docker:dind
  before_script:
   - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  script:
   - cp $ARTIFACT_DIR ./awoo-server
   - docker build -t $DOCKER_IMAGE -f build/awoo-server.dockerfile .
   - docker push $DOCKER_IMAGE

deploy:
  stage: deploy-to-ec2
  image: docker:latest
  tags:
   - gitlab-org-docker
  services:
   - docker:dind
  variables:
    AWOO_IMAGE: $DOCKER_IMAGE
  before_script:
    # to deploy we run a few commands over ssh, private key is stored as variable in the
    # CI/CD settings; maybe not the best approach but good enough for this use case
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # replace | with newline character, stupid workaround for now until I find something better
    - echo $AWS_KEY | tr '|' '\n' > ~/.ssh/aws
    - chmod 600 ~/.ssh/aws
    - ssh-add ~/.ssh/aws
    # write config file to disable host key checking
    - echo "Host *" >> ~/.ssh/config
    - echo "    StrictHostKeyChecking no" >> ~/.ssh/config
    - echo "    IdentityFile ~/.ssh/aws" >> ~/.ssh/config
    - chmod 400 ~/.ssh/config
    # log into registry so we can deploy the image
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
    # install docker-compose for upcoming steps
    - apk add docker-compose
  script:
    # stop running container with docker-compose on remote host
    - docker-compose -H ssh://$EC2_LOGIN -f build/docker-compose.yml kill -s SIGINT
    # start awoo using docker-compose on remote host
    - docker-compose -H ssh://$EC2_LOGIN -f build/docker-compose.yml up -d
