# syntax=docker/dockerfile:1

FROM alpine:3.14
EXPOSE 8080

RUN apk update && apk upgrade

RUN mkdir -p /etc/awoo

WORKDIR /etc/awoo
COPY awoo-server awoo-server
COPY client client

RUN chmod +x awoo-server

CMD ./awoo-server