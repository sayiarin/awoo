let awooAudio = new Audio('resources/awoo.mp3');
let awooTimeout = 1500;

let ws = new WebSocket('wss://awoo.hainsate.com/ws');

ws.onopen = () => {
    console.log('Socket Connection Successful!');
    ws.send('newClient')
}

ws.onclose = event => {
    console.log('Socket Connection Closed: ', event);
}

ws.onerror = error => {
    console.log('Socket Error: ', error);
}

ws.onmessage = message => {
    // we expect the server to only send a number as text
    document.getElementById('totalAwoos').innerText = "Total Global Awoos: " + message.data;
}

function handleClick() {
    setButtonEnabled(false);
    setTimeout(() => setButtonEnabled(true), awooTimeout);
    doAwoo();
}

function setButtonEnabled(enabled) {
    var button = document.getElementById('awooButton');
    if (enabled) {
        button.onclick = handleClick;
        button.className = 'active';
    } else {
        button.onclick = null;
        button.className = 'inactive';
    }
}

function doAwoo() {
    ws.send('awoo');
    awooAudio.play();
}