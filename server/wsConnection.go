package main

import (
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
)

var clients []*websocket.Conn

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     OriginCheck,
}

func OriginCheck(r *http.Request) bool {
	origin := r.Header.Get("Origin")
	return strings.Contains(origin, "localhost") || strings.Contains(origin, "awoo.hainsate.com")
}

func wsReader(conn *websocket.Conn) {
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			// jsut remove client if connection has an error (clinet disconnect also reads as error)
			removeClient(conn)
			return
		} else {
			msgString := string(msg)
			// only increment awoos if actually awoo-ed :>
			if msgString == "awoo" {
				awoos++
				// update all connected clients
				notifyAllClients()
			} else if msgString == "newClient" {
				// register new client and notify of awoo count
				clients = append(clients, conn)
				sendAwooCount(conn)
			}
		}
	}
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println("Client connected")
	wsReader(ws)
}

func notifyAllClients() {
	for _, conn := range clients {
		sendAwooCount(conn)
	}
}

func sendAwooCount(conn *websocket.Conn) {
	err := conn.WriteMessage(websocket.TextMessage, AwoosAsBytes())
	if err != nil {
		log.Println(err)
		return
	}
}

func removeClient(removedConn *websocket.Conn) {
	idx := 0
	for i, conn := range clients {
		if conn == removedConn {
			idx = i
			break
		}
	}
	clients = append(clients[:idx], clients[idx+1:]...)
}

func KeepAliveAllConnections() {
	for _, conn := range clients {
		if err := conn.WriteMessage(websocket.PingMessage, nil); err != nil {
			removeClient(conn)
			conn.Close()
		}
	}
}
