package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var counter = 0

func button(w http.ResponseWriter, r *http.Request) {
	counter++
}

func main() {
	fmt.Println("starting awoo server...")

	// init
	InitAwooCount()

	// create server
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir("./client")))
	mux.HandleFunc("/ws", wsEndpoint)
	server := http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	// setup callback for system interrupt
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	go func() {
		<-interrupt
		SaveAwooCount()
		fmt.Println("shutting down")
		server.Shutdown(context.Background())
	}()

	// setup periodic saves
	ticker := time.NewTicker(5 * time.Minute)
	go func() {
		for {
			select {
			case <-ticker.C:
				SaveAwooCount()
			}
		}
	}()

	// kinda dumb and naïve way of keeping websocket connections alive but good enough for a small project like this
	keepAliveTicker := time.NewTicker(30 * time.Second)
	go func() {
		for {
			select {
			case <-keepAliveTicker.C:
				KeepAliveAllConnections()
			}
		}
	}()

	// start server
	server.ListenAndServe()
}
