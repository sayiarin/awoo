package main

import (
	"os"
	"strconv"
)

var awoos uint64
var awooCountFile = "/etc/awoo/data/awoo.count"

func InitAwooCount() {
	awooFile, err := os.ReadFile(awooCountFile)
	if err != nil {
		return
	}
	// we're going to professionally ignore errors
	awoos, _ = strconv.ParseUint(string(awooFile), 10, 64)
}

func SaveAwooCount() {
	os.WriteFile(awooCountFile, AwoosAsBytes(), 0644)
}

func AwoosAsBytes() []byte {
	return []byte(strconv.FormatUint(awoos, 10))
}
